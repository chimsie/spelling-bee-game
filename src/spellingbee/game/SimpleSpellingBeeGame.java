/**
 * @author Nicolae
 * Simplied version of the backend
 */

package spellingbee.game;

public class SimpleSpellingBeeGame implements ISpellingBeeGame {

	private String letters;
	private int totalScore = 0;
	
	public SimpleSpellingBeeGame() {
		this.letters = "layviot";
	}
	
	@Override
	public int getPointsForWord(String attempt) {
		if (attempt.equals("volatility")) {
			totalScore += 7;
			return 7;
		}
		else if (attempt.equals("totality")) {
			totalScore += 6;
			return 6;
		}
		else if (attempt.equals("lily")) {
			totalScore += 4;
			return 4;
		}
		else if (attempt.equals("vitality")) {
			totalScore += 6;
			return 6;
		}
		else if (attempt.equals("tail")) {
			totalScore += 4;
			return 4;
		}
		return 0;
	}

	@Override
	public String getMessage(String attempt) {
		if (attempt.length() >= 4) {
			return "Nice job!";
		}
		else return "Try again!";
	}

	@Override
	public String getAllLetters() {
		return letters;
	}

	@Override
	public char getCenterLetter() {
		return 'v';
	}

	@Override
	public int getScore() {
		return totalScore;
	}

	@Override
	public int[] getBrackets() {
		int[] thresholdBrackets = new int[5];
		thresholdBrackets[0] = 25;
		thresholdBrackets[1] = 50;
		thresholdBrackets[2] = 80;
		thresholdBrackets[3] = 90;
		thresholdBrackets[4] = 100;
		return thresholdBrackets;
	}
}
