/**
 * @author Jessie
 */

package spellingbee.game;

public interface ISpellingBeeGame {
	
	//returns the number of points that given word is worth
	int getPointsForWord(String attempt);
	
	/*checks if the word attempt is a valid word and returns a message
	 * explaining if the word is rejected or not
	 */
	String getMessage(String attempt);
	
	//returns a set of 7 letters as a String
	String getAllLetters();
	
	//returns the center character that is required to be part of every word
	char getCenterLetter();
	
	//returns the player's current score
	int getScore();
	
	//determines the various point categories
	int[] getBrackets();
}
