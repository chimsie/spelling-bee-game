/**
 * @author Jessie
 * Backend code of the game
 */

package spellingbee.game;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpellingBeeGame implements ISpellingBeeGame{
	//set of 7 letters from which the user will have to choose
	private String letters;
	//character that is required in every word
	private char center;
	//player's score
	private int score;
	//all English words in the dictionary
	List<String> possibleWords;
	//all the words that respect the set of 7 letters and the center letter
	List<String> availableWordsForSet;
	
	/**
	 * Method that creates and returns a list containing all the existing words
	 * @param path is the path to the english.txt file
	 * @return a list of all the words in the english.txt file
	 * @throws IOException
	 */
	public List<String> createWordsFromFile(String path) throws IOException{
		Path p = Paths.get(path);
		this.possibleWords = Files.readAllLines(p);
		return this.possibleWords;
	}
	
	/**
	 * Constructor that initializes all the private variables
	 * @throws IOException
	 */
	public SpellingBeeGame() throws IOException {
		//creates a list containing all the letter combinations
		Path path = Paths.get("Files\\letterCombinations.txt");
		List<String> letterCombinations = Files.readAllLines(path);
		
		//choose a random letter combination
		Random r = new Random();
		this.letters = letterCombinations.get(r.nextInt(letterCombinations.size()));
		//choose a letter that is going the be the center letter from the letter combination chosen
		this.center = this.letters.charAt(r.nextInt(this.letters.length()));
		
		
		this.score = 0;
		
		//stores all English words in a list
		Path path2 = Paths.get("Files\\english.txt");
		this.possibleWords = Files.readAllLines(path2);
		
		//store all the words that respect the set of letters and the center letter in a list
		this.availableWordsForSet = new ArrayList<String>();
		
		for (String word: this.possibleWords) {
			//if the word contains the center letter
			if (word.contains(String.valueOf(this.center))) {
				//if the word contains letters from the set of 7 letters only
				if (containsLetters(word)) {
					this.availableWordsForSet.add(word);
				}
			}
		}
	}
	
	/**
	 * Constructor that initializes all the private fields and uses
	 * @param characters as the set of letters for the game
	 * @throws IOException
	 */
	public SpellingBeeGame(String characters) throws IOException {
		//initializes the set of 7 letters and the center letter from input
		this.letters = characters;
		Random r = new Random();
		this.center = this.letters.charAt(r.nextInt(this.letters.length()));
		this.score = 0;
		Path path = Paths.get("Files\\english.txt");
		this.possibleWords = Files.readAllLines(path);
		this.availableWordsForSet = new ArrayList<String>();
		for (String word: this.possibleWords) {
			if (word.contains(String.valueOf(this.center))) {
				if (containsLetters(word)) {
					this.availableWordsForSet.add(word);
				}
			}
		}
	}
	
	/**
	 * Method that calculates
	 * @param attempt's score and 
	 * @return the score
	 */
	@Override
	public int getPointsForWord(String attempt) {
		if (this.availableWordsForSet.contains(attempt)) {
			if (attempt.length() == 4) {
				return 1;
			}
			else if (attempt.length() > 4 && attempt.length() != 7) {
				return attempt.length();
			}
			else if (attempt.length() == 7) {
				return 7 + 7;
			}
		}
		return 0;
	}
	
	/**
	 * Checks if the 
	 * @param attempt is valid and
	 * @return a message explaining the situation
	 */
	@Override
	public String getMessage(String attempt) {
		if (this.availableWordsForSet.contains(attempt)) {	//if the word exists
			if (attempt.length() >= 4) {	//if the word has at least 4 characters
				this.score += getPointsForWord(attempt);
				return "Great!";
			}
			else {	//if the word has less than 4 characters, it is invalid
				return "Not enough letters! Try again";
			}
		}
		else {	//if the word does not exist
			return attempt + " is not valid! Try again";
		}
	}
	
	/**
	 * @return the set of letters
	 */
	@Override
	public String getAllLetters() {
		return this.letters;
	}
	
	/**
	 * @return the required character
	 */
	@Override
	public char getCenterLetter() {
		return this.center;
	}
	
	/**
	 * @return the score
	 */
	@Override
	public int getScore() {
		return this.score;
	}
	
	/**
	 * @return the various point categories
	 */
	@Override
	public int[] getBrackets() {
		
		int totalPoints = 0;	//will store the maximum points reachable
		
		//checks the number of points for all the words possible
		for (String word: this.availableWordsForSet) {
			totalPoints += getPointsForWord(word);
		}
		
		//store the 25%, 50%, 75%, 90% and 100% points categories in an array
		int[] pointsValue = new int[5];
		pointsValue[0] = (totalPoints * 25)/100;
		pointsValue[1] = (totalPoints * 50)/100;
		pointsValue[2] = (totalPoints * 75)/100;
		pointsValue[3] = (totalPoints * 90)/100;
		pointsValue[4] = totalPoints;
		
		return pointsValue;
	}
	
	/**
	 * Method that verifies if
	 * @param word only contains letters from the set
	 * @return false if the word contains a letter that is not in the set
	 */
	public boolean containsLetters(String word) {
		//split the word in letters
		String[] wordSeparated = word.split("");
		
		//checks if each letter is in the set
		for (String letter: wordSeparated) {
			if (!(this.letters.contains(letter))) {
				return false;
			}
		}
		return true;
	}
}
