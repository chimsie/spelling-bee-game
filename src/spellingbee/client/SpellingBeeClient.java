/**
 * @author Jessie
 * @author Nicolae
 * SpellingBee's interface
 */

package spellingbee.client;

import javafx.application.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.*;


public class SpellingBeeClient extends Application {
	//client that will be used by the game
	private Client client = new Client();
	
	public void start(Stage stage) {
		Group root = new Group(); 
		
		TabPane tp = new TabPane();
		GameTab gameTab = new GameTab(this.client);
		ScoreTab scoreTab = new ScoreTab(this.client);
		
		TextField scorefield = gameTab.getScoreField();
		//change listener on the GameTab's TextField that calls the refresh to update the score on ScoreTab
		scorefield.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldvalue, String newvalue) {
				scoreTab.refresh();
			}
		});
		
		tp.getTabs().addAll(gameTab, scoreTab);
		root.getChildren().add(tp);

		Scene scene = new Scene(root, 225, 300);

		stage.setTitle("Spelling Bee Game"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}