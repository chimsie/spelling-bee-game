/**
 * @author Jessie
 * ScoreTab is in charge of displaying the user's progression
 */

package spellingbee.client;

import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import spellingbee.network.*;

public class ScoreTab extends Tab{
	private Client client;
	private GridPane pane;
	
	//labels
	private Text queenBeeLabel;
	private Text geniusLabel;
	private Text amazingLabel;
	private Text goodLabel;
	private Text startedLabel;
	private Text currentScoreLabel;
	
	//points fields
	private Text queenBee;
	private Text genius;
	private Text amazing;
	private Text good;
	private Text started;
	private Text score;
	
	//points categories
	private int[] points;
	
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		this.pane = new GridPane();
		
		//will store the points categories
		this.points = new int[6];
		//100% category
		this.points[0] = Integer.parseInt(this.client.sendAndWaitMessage("getQueenBee"));
		//90% category
		this.points[1] = Integer.parseInt(this.client.sendAndWaitMessage("getGenius"));
		//75% category
		this.points[2] = Integer.parseInt(this.client.sendAndWaitMessage("getAmazing"));
		//50% category
		this.points[3] = Integer.parseInt(this.client.sendAndWaitMessage("getGood"));
		//25% category
		this.points[4] = Integer.parseInt(this.client.sendAndWaitMessage("getStarted"));
		//user's current score
		this.points[5] = Integer.parseInt(this.client.sendAndWaitMessage("getCurrentScore"));
		
		/* Create the labels
		 * Labels are grayed out until level is reached by the user
		 */
		this.queenBeeLabel = new Text("Queen Bee");
		this.queenBeeLabel.setFill(Color.GREY);
		
		this.geniusLabel = new Text("Genius");
		this.geniusLabel.setFill(Color.GREY);
		
		this.amazingLabel = new Text("Amazing");
		this.amazingLabel.setFill(Color.GREY);
		
		this.goodLabel = new Text("Good");
		this.goodLabel .setFill(Color.GREY);
		
		this.startedLabel = new Text("Getting started");
		this.startedLabel.setFill(Color.GREY);
		
		this.currentScoreLabel = new Text("Current score");
		
		this.pane.add(this.queenBeeLabel, 0, 0);
		this.pane.add(this.geniusLabel, 0, 1);
		this.pane.add(this.amazingLabel, 0, 2);
		this.pane.add(this.goodLabel, 0, 3);
		this.pane.add(this.startedLabel, 0, 4);
		this.pane.add(this.currentScoreLabel, 0, 5);
		
		//show scores categories in their respecting fields
		this.queenBee = new Text(Integer.toString(this.points[0]));
		this.genius = new Text(Integer.toString(this.points[1]));
		this.amazing = new Text(Integer.toString(this.points[2]));
		this.good = new Text(Integer.toString(this.points[3]));
		this.started = new Text(Integer.toString(this.points[4]));
		this.score = new Text(Integer.toString(this.points[5]));
		this.score.setFill(Color.RED);
		
		this.pane.add(queenBee, 1, 0);
		this.pane.add(genius, 1, 1);
		this.pane.add(amazing, 1, 2);
		this.pane.add(good, 1, 3);
		this.pane.add(started, 1, 4);
		this.pane.add(score, 1, 5);
		
		this.pane.setHgap(20);
		
		this.setContent(this.pane);
	}
	
	/**
	 * method that is called every time there is a change in the input TextField from the GameTab
	 * verify and updates the user's score in the tab
	 * update the user's level according to their number of points
	 * 
	 */
	public void refresh() {
		//get the user's new score and update the score shown in the tab
		this.points[5] = Integer.parseInt(this.client.sendAndWaitMessage("getCurrentScore"));;
		this.score.setText(Integer.toString(this.points[5]));
		
		//verify if the user's level increased and update his level
		if (this.points[5] > this.points[4]) {
			this.startedLabel.setFill(Color.BLACK);
		}
		else if(this.points[5] > this.points[3]) {
			this.goodLabel.setFill(Color.BLACK);
		}
		else if(this.points[5] > this.points[2]) {
			this.amazingLabel.setFill(Color.BLACK);
		}
		else if(this.points[5] > this.points[1]) {
			this.geniusLabel.setFill(Color.BLACK);
		}
		else if(this.points[5] == this.points[0]) {
			this.queenBeeLabel.setFill(Color.BLACK);
		}
	}
}
