/**
 * @author Nicolae
 * GameTab is where the user plays
 */
package spellingbee.client;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class GameTab extends Tab{
	private Client client;
	private TextField textField, serverMessage, pointsCount;
	private VBox allItems;
	private HBox buttonBox, utilitiesBox, resultsBox;
	private Button letter1, letter2, letter3, letter4, letter5, letter6, letter7, clearBtn, deleteBtn, submitBtn;
	
	public GameTab(Client client) {
		super("Game");
		this.client = client;
		this.textField = new TextField();
		this.serverMessage = new TextField();
		this.pointsCount = new TextField();		
		this.allItems = new VBox();
		this.buttonBox = new HBox();
		this.resultsBox = new HBox();
		this.utilitiesBox = new HBox();
		this.resultsBox = new HBox();
		
		/*	Creates new buttons with each letter
		 *  Makes the letter red if its the center letter
		 */
			String centerLetter = this.client.sendAndWaitMessage("getCenterLetter");
			this.letter1 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(0,1));
				if (letter1.getText().equals(centerLetter)) {letter1.setTextFill(Color.RED); }
				
			this.letter2 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(1,2));
				if (letter2.getText().equals(centerLetter)) {letter2.setTextFill(Color.RED); }
				
			this.letter3 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(2,3));
				if (letter3.getText().equals(centerLetter)) {letter3.setTextFill(Color.RED); }
				
			this.letter4 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(3,4));
				if (letter4.getText().equals(centerLetter)) {letter4.setTextFill(Color.RED); }
				
			this.letter5 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(4,5));
				if (letter5.getText().equals(centerLetter)) {letter5.setTextFill(Color.RED); }
				
			this.letter6 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(5,6));
				if (letter6.getText().equals(centerLetter)) {letter6.setTextFill(Color.RED); }
				
			this.letter7 = new Button(this.client.sendAndWaitMessage("getAllLetters").substring(6,7));
				if (letter7.getText().equals(centerLetter)) {letter7.setTextFill(Color.RED); }
			
		// Creates new buttons to clear and delete text field + Event Handler
				this.clearBtn = new Button("Clear");
				this.clearBtn.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e) {textField.clear();}});
				
				this.deleteBtn = new Button("Delete");
				this.deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e) {
						if (textField.getLength() != 0) {
							textField.setText(textField.getText(0, textField.getLength()-1));
							}}});
			
		// Submit button, Server message and points count
				this.serverMessage.setMinWidth(180);
				this.serverMessage.setText("Are you a Spelling Bee Queen?");
				this.pointsCount.setMaxWidth(30);
				this.pointsCount.setText("0");
				this.submitBtn = new Button("Submit");
				this.submitBtn.setTextFill(Color.GREEN);
				this.submitBtn.setOnAction(new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e) {
						String[] serverResponse = client.sendAndWaitMessage("isValid;" + textField.getText()).split(";");
						serverMessage.setText(serverResponse[0]);
						pointsCount.setText(client.sendAndWaitMessage("getCurrentScore"));
						textField.clear();
					}
				});
				

		//Event Handler - Append letters to text field
			letter1.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent e) {textField.appendText(letter1.getText());}});
			
			letter2.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter2.getText());}});
			
			letter3.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter3.getText());}});
			
			letter4.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter4.getText());}});
			
			letter5.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter5.getText());}});
			
			letter6.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter6.getText());}});
			
			letter7.setOnAction(new EventHandler<ActionEvent>() {
	            public void handle(ActionEvent e) {textField.appendText(letter7.getText());}});
		
		// Adds everything to the scene
		this.buttonBox.getChildren().addAll(letter1, letter2, letter3, letter4, letter5, letter6, letter7);
		this.utilitiesBox.getChildren().addAll(deleteBtn, clearBtn,  submitBtn);
		this.resultsBox.getChildren().addAll(serverMessage, pointsCount);
		this.allItems.getChildren().addAll(buttonBox, textField, utilitiesBox, resultsBox);
		this.setContent(allItems);
	}
	
	// Returns the score object
	public TextField getScoreField() {
		return this.textField;
	}
}
