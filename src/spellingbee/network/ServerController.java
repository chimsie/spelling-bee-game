package spellingbee.network;

import java.io.IOException;

import spellingbee.game.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	private ISpellingBeeGame spellingBee;
	private int[] scores;
	
	public ServerController() throws IOException {
		this.spellingBee = new SpellingBeeGame();
		this.scores = this.spellingBee.getBrackets();
	}
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 * @author Jessie
	 */
	public String action(String inputLine) {
		if (inputLine.equals("getAllLetters")) {
			return this.spellingBee.getAllLetters();
		}
		else if (inputLine.equals("getCenterLetter")) {
			return Character.toString(this.spellingBee.getCenterLetter());
		}
		else if (inputLine.equals("getQueenBee")){
			int score = spellingBee.getBrackets()[4];
			return Integer.toString(score);
		}
		else if (inputLine.equals("getGenius")){
			return Integer.toString(this.scores[3]);
		}
		else if (inputLine.equals("getAmazing")){
			return Integer.toString(this.scores[2]);
		}
		else if (inputLine.equals("getGood")){
			return Integer.toString(this.scores[1]);
		}
		else if (inputLine.equals("getStarted")){
			return Integer.toString(this.scores[0]);
		}
		else if (inputLine.equals("getCurrentScore")) {
			return Integer.toString(this.spellingBee.getScore());
		}
		else if (inputLine.contains("isValid;")) {
			if (inputLine.length() < 9) {
				return "Oops! Gotta enter a word";
			}
			//splits inputLine in the format "isValid;wordToCheck"
			String[] input = inputLine.split(";");
			if (input[1] == null) { return null; }
			String wordToCheck = input[1];
			//get the validation message from attempt
			String message = this.spellingBee.getMessage(wordToCheck);
			//get the points awarded for the attempt
			int pointsAwarded = this.spellingBee.getPointsForWord(wordToCheck);
			//returns a string in the format "attemptMessage;pointsAwarded"
			return message + ";" + pointsAwarded;
		}
		else {
			return null;
		}
	}
}
